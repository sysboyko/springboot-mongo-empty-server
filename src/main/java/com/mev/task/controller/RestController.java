package com.mev.task.controller;

import com.mev.task.MyException;
import com.mev.task.service.SelectService;
import com.mongodb.DBObject;
import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @Autowired
    private SelectService selectService;

    @RequestMapping(value = "/selectAll", method = RequestMethod.POST)
    @ResponseBody
    public List<DBObject> getAll() {
        return selectService.getAll();
    }

    @RequestMapping(value = "/addNew", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void addNew(@RequestBody String request) {
        JSONObject json = new JSONObject(request);
//        employeeService.add(json.getString("name"),json.getString("address"),json.getInt("age"));
    }

    @RequestMapping(value = "/select", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<String> selectByField(@RequestBody String request) {
        JSONObject json = new JSONObject(request);
        System.out.println(json);
        try {
            return selectService.selectLike(json);
        } catch (MyException myExeption) {
            return Arrays.asList("Syntax ERROR");
        }
    }
}

