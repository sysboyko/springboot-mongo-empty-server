package com.mev.task.entity;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Cars {

    private String name;
    private String brand;
    private int power;
    public Cars(){}

    public Cars(String name, String brand, int power) {
        this.name = name;
        this.brand = brand;
        this.power = power;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
