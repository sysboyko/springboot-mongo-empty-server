package com.mev.task.service.impl;

import com.mev.task.DTO.Condition;
import com.mev.task.MyException;
import com.mev.task.entity.Employee;
import com.mev.task.service.SelectService;

import com.mongodb.*;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Collation;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;
import java.util.*;

@Service
public class SelectServiceImpl
        implements SelectService {
    @Autowired
    private MongoTemplate template;

    @Autowired
    private MongoOperations mongoOperations;


    private final static List<String> operatorsList = Arrays.asList("=", ">", ">=", "<", "<=", "<>");
    private final static Map<String, String> operatorsConvert;

    static {
        operatorsConvert = new HashMap<String, String>();
        operatorsConvert.put("=", "$eq");
        operatorsConvert.put(">", "$gt");
        operatorsConvert.put(">=", "$gte");
        operatorsConvert.put("<", "$lt");
        operatorsConvert.put("<=", "$lte");
        operatorsConvert.put("<>", "$lte");
    }

    @Override
    public List<String> selectLike(JSONObject json) throws MyException {
        List<Bson> listArg = new ArrayList<>();
        //FROM
        MongoCollection<Document> collection = template.getCollection(json.getString("from"));

        List<String> result = new ArrayList<>();
        Block<Document> printBlock = document -> {
            result.add(document.toJson());
            System.out.println(document.toJson());
        };

        //WHERE
        if (json.has("andOrObj")) {
            JSONArray array = json.getJSONArray("andOrObj");
            List<List> orList = new ArrayList<>();

            array.forEach(el -> {
                        if (el instanceof JSONArray) {
                            List<Bson> listAndEmb = new ArrayList<>();
                            ((JSONArray) el).forEach(e ->
                                    {
                                        String operator = "", field = "", val = "";
                                        for (String op : operatorsList) {
                                            if (e.toString().contains(op)) {
                                                operator = op;
                                                field = e.toString().split(op)[0];
                                                val = e.toString().split(op)[1];
                                            }
                                        }
                                        listAndEmb.add(new Document(field, new Document(operatorsConvert.get(operator), val)));
                                    }
                            );
                            List<Bson> bsonList = new ArrayList<>();
                            bsonList.add(new Document("$and", listAndEmb));
                            orList.add(bsonList);
                        } else {
                            String operator = "", field = "", val = "";
                            for (String op : operatorsList) {
                                if (el.toString().contains(op)) {
                                    operator = op;
                                    field = el.toString().split(op)[0];
                                    val = el.toString().split(op)[1];
                                }
                            }
                            List<Bson> listAndEmb = new ArrayList<>();
                            listAndEmb.add(new Document(field, new Document(operatorsConvert.get(operator), val)));
                            orList.add(listAndEmb);
                        }
                    }
            );

            listArg.add(new Document("$or", orList));

        } else {
            if (json.has("and") || json.has("or")) {
                if (json.has("and")) {
                    JSONArray andArr = json.getJSONArray("and");
                    List<String> andCondition = new ArrayList<>();
                    for (int i = 0; i < andArr.length(); i++) {
                        andCondition.add(andArr.get(i).toString());
                    }
                    conditionToDocAddToArgList(listArg, "and", andCondition, json.getString("where"));
                }
                if (json.has("or")) {
                    JSONArray andArr = json.getJSONArray("or");
                    List<String> orCondition = new ArrayList<>();
                    for (int i = 0; i < andArr.length(); i++) {
                        orCondition.add(andArr.get(i).toString());
                    }
                    conditionToDocAddToArgList(listArg, "or", orCondition, json.getString("where"));
                }
            } else {
                if (json.has("where")) {
                    listArg.add(Aggregates.match(where(json.getString("where").trim())));
                }
            }
        }
        if (json.has("orderBy")) {
            List<Document> manyFields = new ArrayList<>();
            int sorting;
            if (json.getString("sort").trim().equals("asc")) {
                sorting = 1;
            } else {
                sorting = -1;
            }
            if (json.get("orderBy") instanceof JSONArray) {
                json.getJSONArray("orderBy").forEach(el -> manyFields.add(new Document(el.toString(), sorting)));
                listArg.add(new Document("$sort", manyFields));
            } else {
                listArg.add(new Document("$sort", new Document(json.getString("orderBy").trim(), sorting)));
            }
        }

        //SKIP
        if (json.has("skip")) {
            if (json.getInt("skip") > 0) {
                listArg.add(new Document("$skip", json.getInt("skip")));
            }
        }
        //limit
        if (json.has("limit")) {
            if (json.getInt("limit") > 0) {
                listArg.add(new Document("$limit", json.getInt("limit")));
            }
        }
        System.err.println(listArg.toString());
        result.add(json.getJSONArray("selectFields").toString());
        if (listArg.size() > 0) {
            collection.aggregate(listArg).forEach(printBlock);
            return result;
        } else {
            collection.find().forEach(printBlock);
            return result;
        }

    }


    private Bson where(String where) {
        String operator = "";
        String field = "";
        String val = "";
        for (String op : operatorsList) {
            if (where.contains(op)) {
                operator = op;
                field = where.split(op)[0];
                val = where.split(op)[1];
            }
        }
        if (val.matches("-?\\d+(\\.\\d+)?")) {
            switch (operator) {
                case "=":
                    return Filters.eq(field, Integer.parseInt(val));
                case ">":
                    return Filters.gt(field, Integer.parseInt(val));
                case ">=":
                    return Filters.gte(field, Integer.parseInt(val));
                case "<":
                    return Filters.lt(field, Integer.parseInt(val));
                case "<=":
                    return Filters.lte(field, Integer.parseInt(val));
                case "<>":
                    return Filters.ne(field, Integer.parseInt(val));
                default:
                    return null;
            }
        } else {
            switch (operator) {
                case "=":
                    return Filters.eq(field, val);
                case ">":
                    return Filters.gt(field, val);
                case ">=":
                    return Filters.gte(field, val);
                case "<":
                    return Filters.lt(field, val);
                case "<=":
                    return Filters.lte(field, val);
                case "<>":
                    return Filters.ne(field, val);
                default:
                    return null;
            }
        }
    }

    void conditionToDocAddToArgList(List<Bson> listArg, String andOrOperator, List<String> andCondition, String where) {
        List<Bson> doc = new ArrayList<>();
        for (String el : andCondition) {
            Condition obj = getConditionObj(el);
            doc.add(new Document(obj.getField(), new Document(operatorsConvert.get(obj.getOperator()), obj.getVal())));
        }

        Condition obj = getConditionObj(where);
        doc.add(new Document(obj.getField(), new Document(operatorsConvert.get(obj.getOperator()), obj.getVal())));

        listArg.add(new Document("$" + andOrOperator, doc));
    }

    Condition getConditionObj(String condition) {
        Condition c = new Condition();
        for (String op : operatorsList) {
            if (condition.contains(op)) {
                c.setOperator(op);
                c.setField(condition.split(op)[0]);
                c.setVal(condition.split(op)[1]);
            }
        }
        return c;
    }


    @Override
    public List<DBObject> getAll() {
        return null;
    }

    @Override
    public void addEmployee(String name, String address, int age) {

    }

    @Override
    public void addCar(String name, String address, int age) {

    }
}