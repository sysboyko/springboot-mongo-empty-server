package com.mev.task.service;



import com.mev.task.MyException;
import com.mongodb.DBObject;
import org.bson.Document;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public interface SelectService {
    List<String> selectLike(JSONObject json) throws MyException;
    List<DBObject> getAll();
    void addEmployee(String name,String address,int age);
    void addCar(String name,String address,int age);

}
