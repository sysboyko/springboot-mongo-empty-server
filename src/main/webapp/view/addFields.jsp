<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="col-lg-6">
<form class="addForm">
    <label for="name">Name</label>
    <input type="text" name="name" id="name">
    <label for="address">Address</label>
    <input type="text" name="address" id="address">
    <label for="age">Age</label>
    <input type="text" name="age" class="age" id="age">
    <input type="submit" value="Отправить" class="btn btn-info">
</form>
</div>
<script>
    $('.age').keypress(function (e) {
        var charCode = e.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=41 && charCode!=40) {
            return false;
        }
        return true;
    });

    $('.addForm').on('submit',function (e) {
        e.preventDefault();
        $.ajax({
            url:"addNew",
            type:"POST",
            contentType: "application/json",
            data: JSON.stringify({name: $('#name').val(), address: $('#address').val(),age:$('#age').val()}),
            success:function (data) {
                console.log("OK")
                console.log(data)
            }
        })
    })
</script>