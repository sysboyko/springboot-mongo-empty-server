<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<form id="selectForm">
    <textarea name="select" id="textarea" cols="30" rows="10"></textarea>
    <input type="submit" value="send" class="btn btn-info" style="text-transform: uppercase;font-weight: bold">
</form>

<div>
    <ul id="result"></ul>
</div>


<script>
    $('#selectForm').on('submit', function (e) {
        e.preventDefault();
        $('.error').css("display", "none");
        var query = $('#textarea').val().toLowerCase().trim();
        while(query.match("  ")){
            query=query.replace("  "," ");
        }
        while(query.match("'")){
            query=query.replace("'","");
        }
        var selectFields = query.split("select")[1].trim().split("from")[0].replace(/\s/g, '').split(",");
        var from = query.split("select")[1].trim().split("from")[1].split(" ")[1].trim();
        var where = "";
        var and = [];
        var or = [];
        var andOrArr = [];
        var orderBy = [];
        var limit = "";
        var sorting = "";
        var skip = "";

        if (query.match("where")) {
            where = query.split("where")[1].trim().split(" ")[0].trim();
        }
        query = query.replace(query.split("where")[0] + "where", "");

        if (query.match("order by")) {
            var order = query.split("order by")[1];
            if (order.match("asc")) {
                query = query.replace("order by" + order.split("asc")[0] + "asc", "");
                order = order.split("asc")[0].trim();
                sorting = "asc";
            }
            if (order.match("desc")) {
                query = query.replace("order by" + order.split("desc")[0] + "desc", "");
                order = order.split("desc")[0].trim();
                sorting = "desc";
            }
            if (order.match(",")) {
                while (order.match(",")) {
                    var field = order.split(",")[0];
                    orderBy.push(order.split(",")[0]);
                    order = order.replace(",", "");
                    order = order.replace(field, "");
                }
                orderBy.push(order);
            } else {
                orderBy.push(order);
            }
        }

        if (query.match("limit")) {
            limit = query.split("limit")[1].trim().split(" ")[0].trim();
            query = query.replace("limit", "");
            query = query.replace(limit, "");

        }
        if (query.match("skip")) {
            skip = query.split("skip")[1].trim().split(" ")[0].trim();
            query = query.replace("skip", "");
            query = query.replace(skip, "");
        }
        // query = query.trim();
console.log(query)
        if (query.match(" and ") && query.match(" or ")) {
            parsingAndOr(query, andOrArr);
        } else {
            if (query.match(" and ")) {
                while (query.match(" and ")) {
                    while (query.match(" and ")) {
                        var andCondition = query.split(" and ")[1].split(" ")[0].trim();
                        and.push(andCondition);
                        query = query.replace(" and ", "");

                    }
                }
            }
            if (query.match(" or ")) {
                while (query.match(" or ")) {
                    var orCondition = query.split(" or ")[1].trim().split(" ")[0].trim();
                    or.push(orCondition);
                    query = query.replace(" or ", "");
                }
            }
        }
        var json = {selectFields: selectFields, from: from};

        if (where.length > 0) {
            json.where = where;
        }

        if (andOrArr.length > 0) {
            json.andOrObj=andOrArr;
        } else {
            if (or.length > 0) {
                json.or = or;
            }
            if (and.length > 0) {
                json.and = and;
            }
        }
        if (orderBy.length > 0) {
            json.orderBy = orderBy;
            json.sort = sorting;
        }
        if (limit.length > 0) {
            json.limit = limit;
        }
        if (skip.length > 0) {
            json.skip = skip;
        }
        console.log("REQUEST: "+JSON.stringify(json));

        $.ajax({
            url: "select",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(json),
            success: function (data) {
                $('#result').empty();
                for(var d in data){
                    $('#result').append('<li>'+data[d]+'</li>');
                }

            }
        })
    });


    function parsingAndOr(query, andOrArr) {
        var leftRightArr = query.split("or");
        var substrLeft = leftRightArr[0];
        var substrRight = "";
        for (var iter = 1; iter < leftRightArr.length - 1; iter++) {
            substrRight += leftRightArr[iter] + "or";        }
        substrRight += leftRightArr[leftRightArr.length - 1];

        if (query.indexOf(" and") < query.indexOf(" or")) {
            substrLeft = substrLeft.split(" and");
            for (var index in substrLeft) {
                substrLeft[index] = substrLeft[index].trim()
            }
            andOrArr.push(substrLeft);
            if (substrRight.match(" or")) {
                parsingAndOr(substrRight, andOrArr)
            } else {
                if (substrRight.match(" and")) {
                    substrRight = query.split(" and");
                    for (var i in substrRight) {
                        substrRight[i] = substrRight[i].trim()
                    }
                } else {
                    substrRight = substrRight.trim();
                }
                andOrArr.push(substrRight);
            }
        } else {
            andOrArr.push(substrLeft);
            if (substrRight.match(" or")) {
                parsingAndOr(substrRight, andOrArr)
            } else {
                if (substrRight.match(" and")) {
                    substrRight = substrRight.split(" and");
                    for (var ind in substrRight) {
                        substrRight[ind] = substrRight[ind].trim();
                    }
                } else {
                    substrRight = substrRight.trim();
                }
                andOrArr.push(substrRight);
            }
        }
    }

</script>
